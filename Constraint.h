#ifndef CONSTRAINT_H
#define CONSTRAINT_H
#include "Particle.h"

class Constraint
{
private:
	float rest_distance; // the length between particle p1 and p2 in rest configuration
	bool active;

	
public:
	Particle *p1, *p2; // the two particles that are connected through this constraint
	
	float getDist(){return rest_distance;};
	
	Constraint(Particle *p1, Particle *p2);
	
	/* This is one of the important methods, where a single constraint between two particles p1 and p2 is solved
	the method is called by Cloth.time_step() many times per frame*/
	void satisfyConstraint();
	
	bool isMember(Particle *pParam);
	
	void desactivar();
	bool getState();
	
};

#endif

