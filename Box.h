#ifndef BOX_H
#define BOX_H
#include "Vec3.h"
#include <GL/gl.h>
#include <GL/glut.h>

class Box {
private:	
	Vec3 position;
	Vec3 dimension;
	Vec3 color;
	bool activo;

public:
	
	Box(Vec3 position ,Vec3 dimension, Vec3 color , bool activo=true) : position(position), dimension(dimension), color(color),activo(activo){};
	
	void Draw();
	Vec3 getPos(){return this->position;}
	Vec3 getDimension(){return this->dimension;};
	bool getActivo(){return this->activo;}
	
};

#endif

