#include "Ball.h"

void Ball::MoveBall(int inv){ //Inv invierte el sentido 1 o -1
	position[2]=inv*cos(time/50.0)*7;
	position[0]=sin(-time*inv/50.0)*3+10;
};

//dibujar la bola
void Ball::Draw(){
	if (this->getActivo()) {
		glPushMatrix(); // to draw the ball we use glutSolidSphere, and need to draw the sphere at the position of the ball
		glTranslatef(this->getPos()[0],this->getPos()[1],this->getPos()[2]);
		
		glColor3f(color[0],color[1],color[2]);		
		glutSolidSphere(this->getRadius()-0.1,50,50);
		glPopMatrix();
	}
};
