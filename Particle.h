#ifndef PARTICLE_H
#define PARTICLE_H
#include "Vec3.h"

/* The particle class represents a particle of mass that can move around in 3D space*/
class Particle
{
	
private:
	float _DAMPING;
	float _TIME_STEPSIZE2;
	int _CONSTRAINT_ITERATIONS;
	bool drawable;
	bool movable; // can the particle move or not ? used to pin parts of the cloth	
	float mass; // the mass of the particle (is always 1 in this example)
	Vec3 pos; // the current position of the particle in 3D space
	Vec3 old_pos; // the position of the particle in the previous time step, used as part of the verlet numerical integration scheme. http://en.wikipedia.org/wiki/Verlet_integration
	Vec3 acceleration; // a vector representing the current acceleration of the particle
	Vec3 accumulated_normal; // an accumulated normal (i.e. non normalized), used for OpenGL soft shading
	bool debil;//para ruptura
	int indiceX;
	int indiceY;
	
	
public:
	void setDamping(float damping){ _DAMPING=damping;};
	void setTimeStep(float time_step){ _TIME_STEPSIZE2=time_step;};
	void setConstraintIter(float constraint_iter){ _CONSTRAINT_ITERATIONS=constraint_iter;};
	void makeUndrawable(){drawable=false;};
	bool isDrawlable(){return drawable;};
	
	void debilitar(){ this->debil = true;};
	bool isDebil(){return debil;}
	int getIndX(){return indiceX;}
	int getIndY(){return indiceY;}
	
	Particle(Vec3 pos) : pos(pos), old_pos(pos),acceleration(Vec3(0,0,0)), mass(1), movable(true), accumulated_normal(Vec3(0,0,0)), drawable(true), debil(false){};
	
	Particle(Vec3 pos,int indx, int indy) : pos(pos), old_pos(pos),acceleration(Vec3(0,0,0)), mass(1), movable(true), accumulated_normal(Vec3(0,0,0)), drawable(true), debil(false),indiceX(indx),indiceY(indy){};
	Particle(){};
	
	void addForce(Vec3 f){ 
		if(drawable)
		   acceleration += f/mass;
	};
	
	/* This is one of the important methods, where the time is progressed a single step size (TIME_STEPSIZE)
	The method is called by Cloth.time_step()
	Given the equation "force = mass * acceleration" the next position is found through verlet integration*/
	void timeStep();
	
	Vec3& getPos() {return pos;}
	
	void setPos(Vec3 position){ this->pos = position;}
	
	void resetAcceleration() {acceleration = Vec3(0,0,0);}
	
	void offsetPos(const Vec3 v) { if(movable) pos += v;}
	
	void makeUnmovable() {movable = false;}
	
	void addToNormal(Vec3 normal)
	{
		if (drawable){
			accumulated_normal += normal.normalized();
		}
	}
	
	Vec3& getNormal() { return accumulated_normal;} // notice, the normal is not unit length
	
	void resetNormal() {accumulated_normal = Vec3(0,0,0);}
	
};

#endif


