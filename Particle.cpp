#include "Particle.h"

void Particle::timeStep(){
	if(movable && drawable){
		Vec3 temp = pos;
		pos = pos + (pos-old_pos)*(1.0-_DAMPING) + acceleration*_TIME_STEPSIZE2;		
		old_pos = temp;
		acceleration = Vec3(0,0,0); // acceleration is reset since it HAS been translated into a change in position (and implicitely into velocity)	
	}
};
