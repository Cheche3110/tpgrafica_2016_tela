/*
Para compilar agregar: -O0 -lGL -lGLU -lglut -fpermissive
Librer�as necesarias en ubuntu: freeglut freeglut-dev

C�digo original: https://web.archive.org/web/20120723224256/http://cg.alexandra.dk/files/MosegaardsClothTutorial.cpp
*/
#ifdef _WIN32
#include <windows.h> 
#endif
#include <GL/gl.h>
#include <GL/glut.h> 
#include <math.h>
#include <vector>
#include <iostream>
#include <iomanip>

#include "Particle.h"
#include "Constraint.h"
#include "Ball.h"
#include "Cloth.h"
#include "Box.h"

using namespace std;

/* Some physics constants */

//#define DAMPING 0.01 // how much to damp the cloth simulation each frame
//#define TIME_STEPSIZE2 0.5*0.5 // how large time step each particle takes each frame
//#define CONSTRAINT_ITERATIONS 15 // how many iterations of constraint satisfaction each frame (more is rigid, less is soft)

float DAMPING=0.01f;
float TIME_STEPSIZE2=0.5*0.5;
int CONSTRAINT_ITERATIONS=4;




// variables globales y defaults
bool cl_info=false;

int
	w=800,h=600, // tama�o de la ventana
	boton=-1, // boton del mouse clickeado
	xclick,yclick, // x e y cuando clickeo un boton
	lod=16; // nivel de detalle (subdivisiones de lineas y superficies parametricas)

float
	escala=20,escala0, // escala de los objetos window/modelo pixeles/unidad
	eye[]={7.5f,7,35}, target[]={7.5f,7,0}, up[]={0,1,0}, // camara, mirando hacia y vertical
	znear=-50, zfar=50;
//	amy,amy0, // angulo del modelo alrededor del eje y
//	ac0,rc0; // angulo resp x y distancia al target de la camara al clickear

GLfloat lightPos[4] = {0,25,-20,0};// posicion luz, l[4]: 0 => direccional -- 1 => posicional

bool // variables de estado de este programa
	luz_camara=true,  // luz fija a la camara o al espacio
//	perspectiva=false, // perspectiva u ortogonal
//	rota=false,       // gira continuamente los objetos respecto de y
	dibuja=true,      // false si esta minimizado
	relleno=false,     // dibuja relleno o no
	smooth=true,      // normales por nodo o por plano
	cullface_wires=false, // usa cull-face para dibujar menos alambres
//	cl_info=true,     // informa por la linea de comandos
	antialias=false,  // antialiasing
	pausa=false;
//	Seleccion=true;	//Rotar pantalla o seleccionar Objeto


// Aca aparecen las declaraciones (globales) del cloth, de 2 bolas y algunas cositas m�s.
Cloth cloth1(14,10,60,60); // ancho, alto, triangulos de ancho, triangulos de alto.
Ball ball1(2,Vec3(1,3,0),Vec3(1,1,0),1.0f,0);
//Ball ball2(2,Vec3(5,8,0),Vec3(1,0,0),1.0f,0);

Box box1(Vec3(0,14,0),Vec3(60,1,20),Vec3(0,0,0.8),true);


bool lines = false,
	fullscreen = false,
	viento = false,
	huracan = false;
//int metodo=0, metodos=2;

short modifiers=0;  // ctrl, alt, shift (de GLUT)

//static const double R2G=45/atan(1.0);

// temporizador:
//static const int ms_lista[]={1,2,5,10,20,50,100,200,500,1000,2000,5000},ms_n=12;
//static int ms_i=4,msecs=ms_lista[ms_i]; // milisegundos por frame
void regen(); //fordward declaration para la funcion de regen (necesaria para Display_cb();)

//------------------------------------------------------------
// Cada vez que hace un redisplay recalcula la posicion de los objetos y las posibles colisiones con la malla
void Display_cb() {
	glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
	
	// ejes
	glBegin(GL_LINES);
		glColor3d(1,0,0); glVertex3d(0,0,0); glVertex3d(1,0,0); //eje x - r
		glColor3d(0,1,0); glVertex3d(0,0,0); glVertex3d(0,1,0); //eje y - g
		glColor3d(0,0,1); glVertex3d(0,0,0); glVertex3d(0,0,1); //eje z - b
	glEnd();
	
	// Calculo de posiciones
	if (!pausa){
		// incrementa el tiempo de las esferas
		ball1.IncrTime(); 
//		ball2.IncrTime();
		// mueve
		ball1.MoveBall(1);
//		ball2.MoveBall(-1);
		
		// propiedades inherentes del cloth
		cloth1.addForce(Vec3(0,0.2,0)*TIME_STEPSIZE2); // gravedad!
		if (viento)
			cloth1.windForce(Vec3(0.5,0.5,0.2)*TIME_STEPSIZE2); // Agrega algo de viento
		if (huracan)
			cloth1.windForce(Vec3(1,1,0.2)*TIME_STEPSIZE2); // agrega un huracan cojonudo!!
		
		cloth1.timeStep(); // Calcula la posici�n de las part�culas en el pr�ximo frame
		
		// interaccion con cuerpos
		if (ball1.getActivo())
			cloth1.ballCollision(ball1.getPos(),ball1.getRadius());
//		if (ball2.getActivo())
//			cloth1.ballCollision(ball2.getPos(),ball2.getRadius()); // Resuelve las colisiones con la esfera2
		if (box1.getActivo())
			cloth1.cubeCollision(box1.getPos(),box1.getDimension());
	}


				cloth1.drawShaded();
	// ojo que las pelotas que se mueven son solamente graficas. no van acompa�ados de las colisiones reales
	// DIBUJA LAS ESFERAS SEGUN SU POSICION CALCULADA ANTES
	ball1.Draw();
//	ball2.Draw();
	box1.Draw();
	
	glutSwapBuffers();

	regen();
	
}

//------------------------------------------------------------
// Regenera la matriz de proyeccion
// cuando cambia algun parametro de la vista
void regen() {
	// matriz de proyeccion
	glMatrixMode(GL_PROJECTION);  glLoadIdentity();
	double w0=(double)w/2/escala,h0=(double)h/2/escala; // semiancho y semialto en el target
	glOrtho(-w0/2,w0/2,h0/2,-h0/2,znear,zfar);  //solamente perspectiva ortogonal

	glMatrixMode(GL_MODELVIEW); glLoadIdentity(); // matriz del modelo->view
	
	
	gluLookAt(  eye[0],   eye[1],   eye[2],
				target[0],target[1],target[2],
				up[0],    up[1],    up[2]);// ubica la camara

//	glRotatef(amy,target[0],target[1],0); // rota alrededor de cloth
	glutPostRedisplay(); // llama a redibujar la pantalla con el loop principal
}

//------------------------------------------------------------
// Animacion

// Si no hace nada hace esto
// glutIdleFunc lo hace a la velocidad que de la maquina
// glutTimerFunc lo hace cada tantos milisegundos

// glutTimerFunc funciona mal, hace cosas raras que son muy visibles
// cuando la espera (msecs) es grande

// El "framerate" real (cuadros por segundo)
// depende de la complejidad del modelo (lod) y la aceleracion por hardware
void Idle_cb() {
	static int suma,counter=0;// esto es para analisis del framerate real
	// Cuenta el lapso de tiempo
	static int anterior=glutGet(GLUT_ELAPSED_TIME); // milisegundos desde que arranco
	if (anterior!=1){ // si msecs es 1 no pierdo tiempo
		int tiempo=glutGet(GLUT_ELAPSED_TIME), lapso=tiempo-anterior;
		if (lapso<anterior) return;
		suma+=lapso;
		if (++counter==100) {
			if(cl_info)cout << "<ms/frame>= " << suma/100.0 << endl;
			counter=suma=0;
		}
		anterior=tiempo;
	}
//	if (rota) { // los objetos giran 1 grado alrededor de y
//		amy+=1; if (amy>360) amy-=360;
//	}else{
		glutPostRedisplay(); // redibujar
//	}
	regen();
}

//------------------------------------------------------------
// Maneja cambios de ancho y alto de la ventana
void Reshape_cb(){
	
	w=glutGet(GLUT_WINDOW_WIDTH);
	h=glutGet(GLUT_WINDOW_HEIGHT);

	
	if (cl_info) cout << "reshape: " << w << "x" << h << endl;

	if (w==0||h==0) {// minimiza
		dibuja=false; // no dibuja mas
		glutIdleFunc(0); // no llama a cada rato a esa funcion
		return;
	}
	else if (!dibuja && w && h){// des-minimiza
		glStencilFunc (GL_EQUAL,1,~0);
		dibuja=true; // ahora si dibuja
//		if (rota) glutIdleFunc(Idle_cb); // registra de nuevo el callback
	}
	
	glViewport(0,0,w,h); // region donde se dibuja
	
	regen(); //regenera la matriz de proyeccion
}

//------------------------------------------------------------
// Teclado
/*
GLUT ACTIVE SHIFT //Set if the Shift modifier or Caps Lock is active.
GLUT ACTIVE CTRL //Set if the Ctrl modifier is active.
GLUT ACTIVE ALT //Set if the Alt modifier is active.
*/
inline short get_modifiers() {return modifiers=(short)glutGetModifiers();}

// Maneja pulsaciones del teclado (ASCII keys)
// x,y posicion del mouse cuando se teclea
void Keyboard_cb(unsigned char key,int x=0,int y=0) {
	switch (key){
		case 'd': DAMPING-=0.01f; cloth1.setDamping(DAMPING); break;
		case 'D': DAMPING+=0.01f; cloth1.setDamping(DAMPING); break;
		case 't': TIME_STEPSIZE2-=.01f;	cloth1.setTimeStep(TIME_STEPSIZE2); break;
		case 'T': TIME_STEPSIZE2+=.01f; cloth1.setTimeStep(TIME_STEPSIZE2); break;
		case '+': CONSTRAINT_ITERATIONS+=1; cloth1.setConstraintIter(CONSTRAINT_ITERATIONS); break;
		case '-': CONSTRAINT_ITERATIONS-=1; cloth1.setConstraintIter(CONSTRAINT_ITERATIONS); break;
		case 'l': case 'L':
			lines = !lines;
			if (lines) glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
			else{ 
				
				glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);}
				
			break;
		case 'f': case 'F':
			fullscreen = !fullscreen;
			if (fullscreen)
				glutFullScreen();
			else
				glutReshapeWindow (w, h);
			break;
			// metodos
		case 'v': case 'V':
			viento = !viento;
			if(cl_info)cout<<"viento: "<<viento<<endl;
			break;
		case 'e':
			/*esfera1 = !esfera1;*/
			ball1.changeActivo();
			if(cl_info)cout<<"Esfera #1: "<<ball1.getActivo()<<endl;
			break;
//		case 'E':
//			ball2.changeActivo();
//			cout<<"Esfera #2: "<<ball2.getActivo()<<endl;
//			break;
		case 'h': case 'H':
			huracan = !huracan;
			if(cl_info)cout<<"Viento fuerte: "<<huracan<<endl;
			break;
		case 'a': case 'A': // Antialiasing
			antialias=!antialias;
			if (antialias){
				glEnable(GL_POINT_SMOOTH); glEnable(GL_LINE_SMOOTH); glEnable(GL_POLYGON_SMOOTH); glEnable(GL_BLEND);
				if (cl_info) cout << "Antialiasing" << endl;
			}
			else {
				glDisable(GL_POINT_SMOOTH); glDisable(GL_LINE_SMOOTH); glDisable(GL_POLYGON_SMOOTH);glDisable(GL_BLEND);
				if (cl_info) cout << "Sin Antialiasing" << endl;
			}
			break;
		case 'i': case 'I': // info
			cl_info=!cl_info;
			cout << ((cl_info)? "Info" : "Sin Info") << endl;
			break;
//		case 'j': case 'J': // luz fija a la camara o en el espacio
//			luz_camara=!luz_camara;
//			if (cl_info)
//				cout << "Luz fija " << ((luz_camara)? "a la camara" : "en el espacio") << endl;
//			regen();
//			break;
//		case 'p': case 'P':  // perspectiva
//			perspectiva=!perspectiva;
//			if (cl_info) cout << ((perspectiva)? "Perspectiva" : "Ortogonal") << endl;
//			break;
		case ' ': // pausa
			pausa=!pausa;
			if (cl_info) cout << ((pausa)? "Pause" : "Play") << endl;
			regen();
			break;
//		case 'r': case 'R': // rotacion
//			rota=!rota;
//			if (cl_info) cout << ((rota)? "Gira" : "No Gira") << endl;
//			break;
		case 's': case 'S': // smooth
			smooth=!smooth;
			glShadeModel((smooth) ? GL_SMOOTH : GL_FLAT);
			if (cl_info) cout << ((smooth)? "Suave" : "Facetado") << endl;
			break;
		case 'q': case 'Q': case 27: // escape => exit
			get_modifiers();
			if (!modifiers)
				exit(EXIT_SUCCESS);
//			Seleccion=!Seleccion;
			break;
		default: break;
	}
	if (!dibuja) glutIdleFunc(0); // no llama a cada rato a esa funcion
	else glutIdleFunc(Idle_cb); // registra el callback
	glutPostRedisplay();
}

// Special keys (non-ASCII)
/*
GLUT KEY F[1,12] F[1,12] function key.
GLUT KEY LEFT Left directional key.
GLUT KEY UP Up directional key.
GLUT KEY RIGHT Right directional key.
GLUT KEY DOWN Down directional key.
GLUT KEY PAGE UP Page up directional key.
GLUT KEY PAGE DOWN Page down directional key.
GLUT KEY HOME Home directional key.
GLUT KEY END End directional key.
GLUT KEY INSERT Inset directional key.
*/
// aca es int key
void Special_cb(int key,int xm=0,int ym=0) {
	if (key==GLUT_KEY_F4){ // alt+f4 => exit
		get_modifiers();
		if (modifiers==GLUT_ACTIVE_ALT)
			exit(EXIT_SUCCESS);
	}
//	if (key==GLUT_KEY_UP||key==GLUT_KEY_DOWN){ // camara
//		// la camara gira alrededor del eje -x
//		double yc=eye[1]-target[1],zc=eye[2]-target[2],
//			rc=sqrt(yc*yc+zc*zc),ac=atan2(yc,zc);
//		ac+=((key==GLUT_KEY_UP) ? 1 : -1)/R2G;
//		yc=rc*sin(ac); zc=rc*cos(ac);
//		up[1]=zc; up[2]=-yc;  // perpendicular
//		eye[1]=target[1]+yc; eye[2]=target[2]+zc;
//		regen();
//	}
//	if (key==GLUT_KEY_LEFT){ // gira
//		amy-=1;
//		regen();
//	}
//	if (key==GLUT_KEY_RIGHT){ // gira
//		amy+=1;
//		regen();
//	}
//	if (key==GLUT_KEY_PAGE_UP||key==GLUT_KEY_PAGE_DOWN){ // velocidad
//		if (key==GLUT_KEY_PAGE_DOWN) ms_i++;
//		else ms_i--;
//		if (ms_i<0) ms_i=0; if (ms_i==ms_n) ms_i--;
//		msecs=ms_lista[ms_i];
//		if (cl_info){
//			if (msecs<1000)
//				cout << 1000/msecs << "fps" << endl;
//			else
//				cout << msecs/1000 << "s/frame)" << endl;
//		}
//	}
}

//------------------------------------------------------------
// Menu
void Menu_cb(int value){
	if(value<256) Keyboard_cb(value); 
	else Special_cb(value-256);
}

//------------------------------------------------------------
// Movimientos del mouse
void Motion_cb(int xm, int ym){ // drag
	if (boton==GLUT_LEFT_BUTTON){
//		if (modifiers==GLUT_ACTIVE_SHIFT){ // cambio de escala
//			escala=escala0*exp((yclick-ym)/100.0);
//			cout<<"Escala: "<<escala<<'\r';
//			regen();
//		}
//		else { // manipulacion
//			double yc=eye[1]-target[1],zc=eye[2]-target[2];
//			double ac=ac0+(ym-yclick)*180.0/h/R2G;
//			yc=rc0*sin(ac); zc=rc0*cos(ac);
//			up[1]=zc; up[2]=-yc;  // perpendicular
//			eye[1]=target[1]+yc; eye[2]=target[2]+zc;
//			amy=amy0+(xm-xclick)*180.0/w;
//			regen();
			xclick=xm; yclick=ym;
			GLint viewport[4];
			GLdouble modelview[16];
			GLdouble projection[16];
			GLfloat winX, winY, winZ;
			GLdouble posX, posY, posZ;
			
			glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
			glGetDoublev( GL_PROJECTION_MATRIX, projection );
			glGetIntegerv( GL_VIEWPORT, viewport );
			
			winX = (float)xm;
			winY = (float)viewport[3] - (float)ym;
			glReadPixels( xm, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
			
			gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
			
			if (cl_info) {
				cout<<"Coord Clic: "<<winX<<" "<<winY<<" "<<winZ<<endl;
				cout<<"Coord Objeto: "<<posX<<" "<<posY<<" "<<posZ<<endl;
			}
			//---------
			
			//si los objetos se mueven se rompe
			cloth1.destroyConstraint(posX, posY, posZ);		
//		}
	}
}

// Clicks del mouse
// GLUT LEFT BUTTON, GLUT MIDDLE BUTTON, or GLUT RIGHT BUTTON
// The state parameter is either GLUT UP or GLUT DOWN
// glutGetModifiers may be called to determine the state of modifier keys
void Mouse_cb(int button, int state, int x, int y){
	
	static bool old_rota=false;
	
	if (button==GLUT_LEFT_BUTTON){//clic izquierdo
		
//		if(Seleccion == false){//rotar objeto			
//			if (state==GLUT_DOWN) {
//				if (cl_info) cout<<" seleccion de obejto desactivada"<<endl;
//				xclick=x; yclick=y;
//				boton=button;
//				old_rota=rota; rota=false;
//				get_modifiers();
//				glutMotionFunc(Motion_cb); // callback para los drags
//				if (modifiers==GLUT_ACTIVE_SHIFT){ // cambio de escala
//					escala0=escala;
//				}
//				else { // manipulacion
//					double yc=eye[1]-target[1],zc=eye[2]-target[2];
//					rc0=sqrt(yc*yc+zc*zc); ac0=atan2(yc,zc);
//					amy0=amy;
//				}
//			}
//			else if (state==GLUT_UP){
//				rota=old_rota;
//				boton=-1;
//				glutMotionFunc(0); // anula el callback para los drags
//			}
//			
//		}else{// seleccionar objeto
			
			if (state==GLUT_DOWN) {

				if (cl_info) cout<<" seleccion de obejto activada"<<endl;
				xclick=x; yclick=y;
				boton=button;
				
				GLint viewport[4];
				GLdouble modelview[16];
				GLdouble projection[16];
				GLfloat winX, winY, winZ;
				GLdouble posX, posY, posZ;
				
				glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
				glGetDoublev( GL_PROJECTION_MATRIX, projection );
				glGetIntegerv( GL_VIEWPORT, viewport );
				
				winX = (float)x;
				winY = (float)viewport[3] - (float)y;
				glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );
				
				gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
				
				if (cl_info) {
					cout<<"Coord Clic: "<<winX<<" "<<winY<<" "<<winZ<<endl;
					cout<<"Coord Objeto: "<<posX<<" "<<posY<<" "<<posZ<<endl;
				}
				//---------
				
				//si los objetos se mueven se rompe
				cloth1.destroyConstraint(posX, posY, posZ);				
				glutMotionFunc(Motion_cb); // callback para los drags
			}	else if (state==GLUT_UP){
				boton=-1;
				glutMotionFunc(0); // anula el callback para los drags
			}
		}
//	}
}

//------------------------------------------------------------
// pregunta a OpenGL por el valor de una variable de estado
int integerv(GLenum pname){
	int value;
	glGetIntegerv(pname,&value);
	return value;
}
#define _PRINT_INT_VALUE(pname) #pname << ": " << integerv(pname) <<endl

//------------------------------------------------------------
// Inicializa GLUT y OpenGL
void initialize() {
	// pide z-buffer, color RGBA y double buffering
	glutInitDisplayMode(GLUT_DEPTH|GLUT_RGBA|GLUT_DOUBLE);

	
	glutInitWindowSize(w,h);
	glutInitWindowPosition(0,0);
	
	glutCreateWindow("TP Final CG 2016"); // crea el main window
	
	//declara los callbacks
	//los que no se usan no se declaran
	glutDisplayFunc(Display_cb); // redisplays
	glutReshapeFunc(Reshape_cb); // cambio de alto y ancho
	glutKeyboardFunc(Keyboard_cb); // teclado
	glutSpecialFunc(Special_cb); // teclas especiales
	glutMouseFunc(Mouse_cb); // botones picados
	if (!dibuja) glutIdleFunc(0); // no llama a cada rato a esa funcion
	else glutIdleFunc(Idle_cb); // registra el callback
	
	// crea el menu
	glutCreateMenu(Menu_cb);
		glutAddMenuEntry("     [+]_Incrementa la iteracion de restricciones", '+');
		glutAddMenuEntry("     [-]_Decrementa la iteracion de restricciones", '-');
		glutAddMenuEntry("     [E]_Muestra/oculta esfera1", 'E');
//		glutAddMenuEntry("     [e]_Muestra/oculta esfera2", 'e');
		glutAddMenuEntry("     [f]_Fullscreen        ", 'f');
//		glutAddMenuEntry("     [p]_Perspectiva/Ortogonal ", 'p');
//		glutAddMenuEntry("     [r]_Rota                  ", 'r');
		glutAddMenuEntry("     [l]_Muestra/oculta las lineas de los objetos", 'l');
		glutAddMenuEntry("     [s]_Suave/Facetado        ", 's');
		glutAddMenuEntry("     [a]_Antialiasing          ", 'a');
		glutAddMenuEntry("     [v]_Viento on/off", 'v');
		glutAddMenuEntry("     [H]_Huracan (Viento fuerte)", 'h');
		glutAddMenuEntry("     [t]_Cambia el TIME_STEPSIZE", 't');
//		glutAddMenuEntry("    [Up]_Sube Camara           ", (256+GLUT_KEY_UP));
//		glutAddMenuEntry("  [Down]_Baja Camara           ", (256+GLUT_KEY_DOWN));
//		glutAddMenuEntry("  [Left]_Gira objeto           ", (256+GLUT_KEY_LEFT));
//		glutAddMenuEntry(" [Right]_Gira objeto           ", (256+GLUT_KEY_RIGHT));
//		glutAddMenuEntry("  [PgUp]_Aumenta Framerate     ", (256+GLUT_KEY_PAGE_UP));
//		glutAddMenuEntry("  [Pgdn]_Disminuye Framerate   ", (256+GLUT_KEY_PAGE_DOWN));
//		glutAddMenuEntry("     [j]_Luz fija ON/OFF       ", 'j');
		glutAddMenuEntry("     [i]_Info ON/OFF           ", 'i');
		glutAddMenuEntry("   [Esc]_Exit                  ", 27);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
	
	// ========================
	// estado normal del OpenGL
	// ========================
	
	glEnable(GL_DEPTH_TEST); glDepthFunc(GL_LEQUAL); // habilita el z-buffer
	glEnable(GL_POLYGON_OFFSET_FILL); glPolygonOffset (1,1); // coplanaridad// habilita el z-buffer
	glEnable(GL_NORMALIZE); // normaliza las normales para que el scaling no moleste
	
	// para el stencil
//	glEnable(GL_STENCIL_TEST);
//	glClearStencil(0); // al borrar el stencil lo llena de unos
//	glStencilOp (GL_KEEP, GL_KEEP, GL_KEEP); // normalmente no escribe en el stencil
	
	// interpola normales por nodos o una normal por plano
	glShadeModel((smooth) ? GL_SMOOTH : GL_FLAT);
	
	// antialiasing
	if (antialias){
		glEnable(GL_POINT_SMOOTH); glEnable(GL_LINE_SMOOTH); glEnable(GL_POLYGON_SMOOTH); glEnable(GL_BLEND);
	}
	else {
		glDisable(GL_POINT_SMOOTH); glDisable(GL_LINE_SMOOTH); glDisable(GL_POLYGON_SMOOTH);glDisable(GL_BLEND);
	}
//	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
//	glHint(GL_POINT_SMOOTH_HINT,GL_NICEST);
//	glHint(GL_LINE_SMOOTH_HINT,GL_NICEST);
//	glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);
	glEnable(GL_BLEND); // Enable the OpenGL Blending functionality  
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	// color de fondo
	glClearColor(.0, .0, .0, .0);
	
	// Aca arranca todo el tema de la iluminacion (no tocar que anda bien)	
	glShadeModel(GL_SMOOTH);			
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_COLOR_MATERIAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	
	glLightfv(GL_LIGHT0,GL_POSITION,(GLfloat *) &lightPos);
	
	glEnable(GL_LIGHT1);
	
	GLfloat lightAmbient1[4] = {0.0,0.0,0.0,0.0};
	GLfloat lightPos1[4] = {1.0,0.0,-0.2,0.0};
	GLfloat lightDiffuse1[4] = {0.5,0.5,0.3,0.0};
	
	glLightfv(GL_LIGHT1,GL_POSITION,(GLfloat *) &lightPos1);
	glLightfv(GL_LIGHT1,GL_AMBIENT,(GLfloat *) &lightAmbient1);
	glLightfv(GL_LIGHT1,GL_DIFFUSE,(GLfloat *) &lightDiffuse1);
	
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE,GL_TRUE);
	
	// ========================
	// info
	if (cl_info)
		cout << "Vendor:         " << glGetString(GL_VENDOR) << endl
		<< "Renderer:       " << glGetString(GL_RENDERER) << endl
		<< "GL_Version:     " << glGetString(GL_VERSION) << endl
		;
	
	regen(); // la funcion de regen es la que hace todo
}

//------------------------------------------------------------
// main
int main(int argc,char** argv) {
	cloth1.setConstraintIter(CONSTRAINT_ITERATIONS);
	cloth1.setTimeStep(TIME_STEPSIZE2);
	cloth1.setDamping(DAMPING);
	
	glutInit(&argc,argv);// inicializa glut
	glTranslatef(0,0,0);
	initialize(); 		// condiciones iniciales de la ventana y OpenGL
	glutMainLoop(); 	// entra en loop de reconocimiento de eventos
	return 0;
}
