#ifndef BALL_H
#define BALL_H
#include "Vec3.h"
#include <GL/gl.h>
#include <GL/glut.h>

class Ball{
private:
	int radius;
	float time;
	Vec3 position; //xyz
	Vec3 color; //rgba que no tiene el alpha por ser de 3
	float alpha;
	bool activo;
public:
	//Particle(Vec3 pos) : pos(pos), old_pos(pos),acceleration(Vec3(0,0,0)), mass(1), movable(true), accumulated_normal(Vec3(0,0,0)){}
	
	Ball(int radius, Vec3 position , Vec3 color , float alpha = 1.0f, float time = 0.0f, bool act = false) : radius(radius), position(position), color(color), alpha(alpha),time(time), activo(act){}
	
	/* used to detect and resolve the collision of the cloth with the ball.
	This is based on a very simples scheme where the position of each particle is simply compared to the sphere and corrected.
	This also means that the sphere can "slip through" if the ball is small enough compared to the distance in the grid bewteen particles
	*/
	//	void ballCollision(Particle particle){
	//		Vec3 v = particle.getPos()-position;
	//		float l = v.length();
	//		if ( v.length() < radius) // if the particle is inside the ball
	//		{
	//			particle.offsetPos(v.normalized()*(radius-l)); // project the particle to the surface of the ball
	//		}
	//	}
	
	void IncrTime(void){this->time++;}		
	Vec3 getPos(){return this->position;}
	float getRadius(){return this->radius;}
	Vec3 getColor(){return this->color;}
	bool getActivo(){return this->activo;}
	void changeActivo(){this->activo = !this->activo;}	
	
	void MoveBall(int inv);//No esta pensado para hacer muchas pelotitas
	void Draw();//dibujar la bola
	
};

#endif

