#include "Constraint.h"
#include <iostream>
#include <cstdlib>
#include <GL/gl.h>

Constraint::Constraint(Particle *p1, Particle *p2) :  p1(p1),p2(p2){
	if ((p1->isDrawlable())&&(p2->isDrawlable())){
		Vec3 vec = p1->getPos()-p2->getPos();
		rest_distance = vec.length();
	}
	this->active = true;

};



void Constraint::satisfyConstraint(){
	if ((p1->isDrawlable())&&(p2->isDrawlable())&&(this->active)){
		Vec3 p1_to_p2 = p2->getPos()-p1->getPos(); // vector from p1 to p2
		float current_distance = p1_to_p2.length(); // current distance between p1 and p2		
		Vec3 correctionVector = p1_to_p2*(1 - rest_distance/current_distance); // The offset vector that could moves p1 into a distance of rest_distance to p2
		//aplicar restriccion o romper
		float distComp=2;
		int dx = abs(p1->getIndX() - p2->getIndX());
		int dy = abs(p1->getIndY() - p2->getIndY());
		bool esSecundaria;
		if(dx==2 or dy==2){
			esSecundaria=true;
		}else{
			esSecundaria=false;
		}

		if(esSecundaria){
			if (p1->isDebil() or p2->isDebil()){
				distComp=1;		
			}else{
				distComp=2;
			}
		}else{
			if (p1->isDebil() and p2->isDebil()){
				distComp=0.7;
			}else{
				distComp=5;
			}
		}
		
		if (current_distance < distComp){
			Vec3 correctionVectorHalf = correctionVector*0.5; // Lets make it half that length, so that we can move BOTH p1 and p2.
			

			p1->offsetPos(correctionVectorHalf); // correctionVectorHalf is pointing from p1 to p2, so the length should move p1 half the length needed to satisfy the constraint.
			p2->offsetPos(-correctionVectorHalf); // we must move p2 the negative direction of correctionVectorHalf since it points from p2 to p1, and not p1 to p2.		
			glColor3f(0.2f,0.7f,0.2f);
			glBegin(GL_LINES);
			Vec3 tmp=p1->getPos();
			tmp[2]-=.5f;
			Vec3 tmp2=p2->getPos();
			tmp2[2]-=.5f;
			glVertex3fv((GLfloat *) &tmp);
			glVertex3fv((GLfloat *) &tmp2 );
			glEnd();			
			
		}else{

			p1->makeUndrawable();
			p2->makeUndrawable();
		}
	}
};

bool Constraint::isMember(Particle *pParam){
	return pParam == p1 or pParam == p2;
}

void Constraint::desactivar(){
	this->active = false;
}

bool Constraint::getState(){
	return this->active;
}
