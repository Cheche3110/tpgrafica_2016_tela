#include "Cloth.h"
#include <iostream>
#include <cstddef>
extern bool cl_info;

Vec3 Cloth::calcTriangleNormal(Particle *p1,Particle *p2,Particle *p3){
	Vec3 pos1 = p1->getPos();
	Vec3 pos2 = p2->getPos();
	Vec3 pos3 = p3->getPos();
	
	Vec3 v1 = pos2-pos1;
	Vec3 v2 = pos3-pos1;
	
	return v1.cross(v2);
};

void  Cloth::addWindForcesForTriangle(Particle *p1,Particle *p2,Particle *p3, const Vec3 direction){
	Vec3 normal = calcTriangleNormal(p1,p2,p3);
	Vec3 d = normal.normalized();
	Vec3 force = normal*(d.dot(direction));
	p1->addForce(force);
	p2->addForce(force);
	p3->addForce(force);
};

void Cloth::drawTriangle(Particle *p1, Particle *p2, Particle *p3, const Vec3 color){
	glColor3fv( (GLfloat*) &color );
		
	
	if(p1->isDrawlable() && p2->isDrawlable() && p3->isDrawlable()){
		
		glBegin(GL_TRIANGLES);
		glNormal3fv((GLfloat *) &(p1->getNormal().normalized() ));
		glVertex3fv((GLfloat *) &(p1->getPos() ));
	
		glNormal3fv((GLfloat *) &(p2->getNormal().normalized() ));
		glVertex3fv((GLfloat *) &(p2->getPos() ));

		glNormal3fv((GLfloat *) &(p3->getNormal().normalized() ));
		glVertex3fv((GLfloat *) &(p3->getPos() ));
		glEnd();
	}
//	else{
//		glColor3d(1,0,0);
//		if(p1->isDrawlable() and p2->isDrawlable() and !p3->isDrawlable()){
//			glBegin(GL_LINES);
//			glVertex3fv((GLfloat *) &(p1->getPos() ));
//			glVertex3fv((GLfloat *) &(p2->getPos() ));
//			glEnd();
//		}
//		
//		if(p1->isDrawlable() and p3->isDrawlable() and !p2->isDrawlable()){
//			glBegin(GL_LINES);
//			glVertex3fv((GLfloat *) &(p1->getPos() ));
//			glVertex3fv((GLfloat *) &(p3->getPos() ));
//			glEnd();			
//		}
//		
//		if(p3->isDrawlable() and p2->isDrawlable() and !p1->isDrawlable()){
//			glBegin(GL_LINES);
//			glVertex3fv((GLfloat *) &(p3->getPos() ));
//			glVertex3fv((GLfloat *) &(p2->getPos() ));
//			glEnd();
//		}
//	}
//	else{
//		glColor3d(1,1,1);
//		glBegin(GL_TRIANGLES);
//		glNormal3fv((GLfloat *) &(p1->getNormal().normalized() ));
//		glVertex3fv((GLfloat *) &(p1->getPos() ));
//		
//		glNormal3fv((GLfloat *) &(p2->getNormal().normalized() ));
//		glVertex3fv((GLfloat *) &(p2->getPos() ));
//		
//		glNormal3fv((GLfloat *) &(p3->getNormal().normalized() ));
//		glVertex3fv((GLfloat *) &(p3->getPos() ));		
//		glEnd();
//	}

	
	
};

void Cloth::setDamping(float damping){ 
	_DAMPING=damping; 
	std::vector<Particle>::iterator particle;
	for(particle = particles.begin(); particle != particles.end(); particle++){
		(*particle).setDamping(damping);
	}
};

void Cloth::setTimeStep(float time_step){ 
	_TIME_STEPSIZE2=time_step;
	std::vector<Particle>::iterator particle;
	for(particle = particles.begin(); particle != particles.end(); particle++){
		(*particle).setTimeStep(time_step);
	}
};

void Cloth::setConstraintIter(float constraint_iter){ 
	_CONSTRAINT_ITERATIONS=constraint_iter;
	std::vector<Particle>::iterator particle;
	for(particle = particles.begin(); particle != particles.end(); particle++){	
		(*particle).setConstraintIter(constraint_iter);
	}
};

Cloth::Cloth(float width, float height, int num_particles_width, int num_particles_height) : num_particles_width(num_particles_width), num_particles_height(num_particles_height){

	xant=-1;yant=-1;
	particles.resize(num_particles_width*num_particles_height); //I am essentially using this vector as an array with room for num_particles_width*num_particles_height particles
	
	// creating particles in a grid of particles from (0,0,0) to (width,-height,0)
	for(int x=0; x<num_particles_width; x++){
		for(int y=0; y<num_particles_height; y++){
			Vec3 pos = Vec3(width * (x/(float)num_particles_width),	height * (y/(float)num_particles_height),0);
			particles[y*num_particles_width+x]= Particle(pos,x,y); // insert particle in column x at y'th row
		}
	}
	
	// Connecting immediate neighbor particles with constraints (distance 1 and sqrt(2) in the grid)
	for(int x=0; x<num_particles_width; x++){
		for(int y=0; y<num_particles_height; y++){
			if (x<num_particles_width-1) makeConstraint(getParticle(x,y),getParticle(x+1,y)); //derecha
			if (y<num_particles_height-1) makeConstraint(getParticle(x,y),getParticle(x,y+1));//abajo
			if (x<num_particles_width-1 && y<num_particles_height-1) makeConstraint(getParticle(x,y),getParticle(x+1,y+1)); //diagonal
//			if (x<num_particles_width-1 && y<num_particles_height-1) makeConstraint(getParticle(x+1,y),getParticle(x,y+1));
		}
	}
	
	// Connecting secondary neighbors with constraints (distance 2 and sqrt(4) in the grid)
	for(int x=0; x<num_particles_width; x++){
		for(int y=0; y<num_particles_height; y++){
			if (x<num_particles_width-2) makeConstraint(getParticle(x,y),getParticle(x+2,y)); //derecha
			if (y<num_particles_height-2) makeConstraint(getParticle(x,y),getParticle(x,y+2)); //arriba
			if (x<num_particles_width-2 && y<num_particles_height-2) makeConstraint(getParticle(x,y),getParticle(x+2,y+2));
			
		}
	}
	
	// fija todas las particulas superiores (no movibles)
	for(int i=0;i<=num_particles_width; i++)	{
		getParticle(0+i ,0)->offsetPos(Vec3(0.0,0.0,0.0)); // moving the particle a bit towards the center, to make it hang more natural - because I like it ;)
		getParticle(0+i ,0)->makeUnmovable(); 
	}
};


void Cloth::drawShaded(){
	// reset normals (which where written to last frame)
	std::vector<Particle>::iterator particle;
	for(particle = particles.begin(); particle != particles.end(); particle++){
		(*particle).resetNormal();
		
		//parche
		if(!particle->isDrawlable()){
			int x=particle->getIndX();
			int y=particle->getIndY();
			Particle *pizq=getParticle(x-1,y);
			Particle *pder=getParticle(x+1,y);
			Particle *psup=getParticle(x,y-1);
			Particle *pinf=getParticle(x,y+1);
			Particle *pdiaginf=getParticle(x-1,y-1);
			Particle *pdiagsup=getParticle(x+1,y+1);		
			Particle *pdiaginf2=getParticle(x-1,y+1);
			Particle *pdiagsup2=getParticle(x+1,y-1);		
			//debilitando
			if(pizq && pder && psup && pinf && pdiaginf && pdiaginf2 && pdiagsup && pdiagsup2){
				if(!pizq->isDrawlable()){
					
					pder->debilitar();
				}
				if(!pder->isDrawlable()){
					pizq->debilitar();
				}				   
			}
//			if(pizq)pizq->debilitar();
//			if(pder)pder->debilitar();
//			if(psup)psup->debilitar();
//			if(pinf)pinf->debilitar();
//			if(pdiaginf)pdiaginf->debilitar();
//			if(pdiaginf2)pdiaginf2->debilitar();
//			if(pdiagsup)pdiagsup->debilitar();
//			if(pdiagsup2)pdiagsup2->debilitar();
		}
		//finparche
	}
	
	//create smooth per particle normals by adding up all the (hard) triangle normals that each particle is part of
	for(int x = 0; x<num_particles_width-1; x++){
		for(int y=0; y<num_particles_height-1; y++){
			Vec3 normal = calcTriangleNormal(getParticle(x+1,y),getParticle(x,y),getParticle(x,y+1));
			getParticle(x+1,y)->addToNormal(normal);
			getParticle(x,y)->addToNormal(normal);
			getParticle(x,y+1)->addToNormal(normal);
			
			normal = calcTriangleNormal(getParticle(x+1,y+1),getParticle(x+1,y),getParticle(x,y+1));
			getParticle(x+1,y+1)->addToNormal(normal);
			getParticle(x+1,y)->addToNormal(normal);
			getParticle(x,y+1)->addToNormal(normal);
		}
	}
	
//	glBegin(GL_TRIANGLES);
	for(int x = 0; x<num_particles_width-1; x++){
		for(int y=0; y<num_particles_height-1; y++){
			
			bool test=getParticle(x,y)->isDrawlable();
			test= test && getParticle(x+1,y)->isDrawlable();
			test= test && getParticle(x+1,y+1)->isDrawlable();

			if(test){//cambie orden para representar los constraints
				Vec3 color(0.2f,0.7f,0.2f);	
			
				
				drawTriangle(getParticle(x+1,y),getParticle(x,y),getParticle(x+1,y+1),color);
				drawTriangle(getParticle(x,y),getParticle(x,y+1),getParticle(x+1,y+1),color);
				
			}
//			else{//comentar estas lineas. es solo para chequear borrado de restricciones
//				Vec3 color(1.f,0.f,0.f);	
//				drawTriangle(getParticle(x+1,y),getParticle(x,y),getParticle(x+1,y+1),color);
//				drawTriangle(getParticle(x,y),getParticle(x,y+1),getParticle(x+1,y+1),color);
//			}
		}
	}
//	glEnd();

};

void Cloth::timeStep(){
	std::vector<Constraint>::iterator constraint;
	for(int i=0; i<_CONSTRAINT_ITERATIONS; i++){ // iterate over all constraints several times
	
		for(constraint = constraints.begin(); constraint != constraints.end(); constraint++ ){
			
			(*constraint).satisfyConstraint(); // satisfy constraint.			
		}
	}
	
	std::vector<Particle>::iterator particle;
	for(particle = particles.begin(); particle != particles.end(); particle++){
		(*particle).timeStep(); // calculate the position of each particle at the next time step.		
	}
};

void Cloth::addForce(const Vec3 direction){
	std::vector<Particle>::iterator particle;
	for(particle = particles.begin(); particle != particles.end(); particle++){
		(*particle).addForce(direction); // add the forces to each particle
	}
	
};

void Cloth::windForce(const Vec3 direction){
	for(int x = 0; x<num_particles_width-1; x++){
		for(int y=0; y<num_particles_height-1; y++){
			addWindForcesForTriangle(getParticle(x+1,y),getParticle(x,y),getParticle(x,y+1),direction);
			addWindForcesForTriangle(getParticle(x+1,y+1),getParticle(x+1,y),getParticle(x,y+1),direction);
		}
	}
}

void Cloth::cubeCollision(const Vec3 position,const Vec3 dimension ){
	std::vector<Particle>::iterator particle;
	for(particle = particles.begin(); particle != particles.end(); particle++){

		Vec3 v = (*particle).getPos();			
		float techo=position[1];

		if ( v[1] > techo){ 
			v[1]=techo;
			(*particle).setPos(v);
		}
	}
};



void Cloth::ballCollision(const Vec3 center,const float radius ){
	std::vector<Particle>::iterator particle;
	for(particle = particles.begin(); particle != particles.end(); particle++){
		Vec3 v = (*particle).getPos()-center;
		float l = v.length();
		if ( v.length() < radius){ // if the particle is inside the ball		
			(*particle).offsetPos(v.normalized()*(radius-l)); // project the particle to the surface of the ball
		}
	}
};

void Cloth::destroyConstraint(float pos3D_x,float pos3D_y,float pos3D_z){
	
	Particle *p,*pizq,*pder,*psup,*pinf,*pdiagsup,*pdiaginf;
	int x=0,y=0;//de la particula seleccionada
	
	Vec3 vparam= Vec3(pos3D_x,pos3D_y,pos3D_z);
	
	p=BuscarParticula(vparam,x,y);
	if (!p) return;
	p->makeUndrawable();	

	
	//desactivar a P
	std::vector<Constraint>::iterator iterConstraint;
	int cont=0;
	for(iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
		if(iterConstraint->isMember(p)){
			cont+=1;
			iterConstraint->desactivar();
		}
	}
	
	
	if(cl_info) std::cout<<"afectadas por P: "<<cont<<"\n";
	
	
	if(yant==-1 && xant==-1){
		xant=x;yant=y;
	}
	   
	
	if ( (x>0&&x<num_particles_width-1) && (y>0 && y<num_particles_height-1)){
		pizq=getParticle(x-1,y);
		pder=getParticle(x+1,y);
		psup=getParticle(x,y-1);
		pinf=getParticle(x,y+1);
		pdiaginf=getParticle(x-1,y-1);
		pdiagsup=getParticle(x+1,y+1);
		
		Particle *pdiaginf2=getParticle(x-1,y+1);
		Particle *pdiagsup2=getParticle(x+1,y-1);
		
		//debilitando
		pizq->debilitar();
		pder->debilitar();
		psup->debilitar();
		pinf->debilitar();
		pdiaginf->debilitar();
		pdiaginf2->debilitar();
		pdiagsup->debilitar();
		pdiagsup2->debilitar();
		
		if(cl_info){
			std::cout<<"P "<<p->getPos()[0]<<" "<<p->getPos()[1]<<"\n";
			std::cout<<"P[xy]"<<x<<" "<<y<<"\n";
			
			std::cout<<"Pder "<<pder->getPos()[0]<<" "<<pder->getPos()[1]<<"\n";
			std::cout<<"Pizq "<<pizq->getPos()[0]<<" "<<pizq->getPos()[1]<<"\n";
			std::cout<<"Psup "<<psup->getPos()[0]<<" "<<psup->getPos()[1]<<"\n";
			std::cout<<"pinf "<<pinf->getPos()[0]<<" "<<pinf->getPos()[1]<<"\n";
			std::cout<<"pdiaginf "<<pdiaginf->getPos()[0]<<" "<<pdiaginf->getPos()[1]<<"\n";
			std::cout<<"pdiagsup "<<pdiagsup->getPos()[0]<<" "<<pdiagsup->getPos()[1]<<"\n";	
			
			
			std::cout<<"contador afecta p "<<cont<<"\n";
		}
		
		cont=0;
		for(iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
			if(iterConstraint->isMember(pder) && iterConstraint->isMember(pizq)){
				cont+=1;
				//			constraints.erase(iterConstraint);
				iterConstraint->desactivar();
			}
			if(iterConstraint->isMember(psup) && iterConstraint->isMember(pinf)){
				cont+=1;
				//			constraints.erase(iterConstraint);
				iterConstraint->desactivar();
			}
			if(iterConstraint->isMember(pdiaginf) && iterConstraint->isMember(pdiagsup)){
				cont+=1;
				//			constraints.erase(iterConstraint);
				iterConstraint->desactivar();
			}
		}
		if(cl_info)std::cout<<"contador no afecta p "<<cont<<"\n";
		
		
		if(x==xant+1 && y==yant-1){
			if(cl_info)std::cout<<"subi diagonal hacia derecha\n";
			try{
				Particle *ptemp1=getParticle(x-1,y);
				Particle *ptemp2=getParticle(x,y+1);
				
				Particle *ptemp3=getParticle(x-2,y-1);
				Particle *ptemp4=getParticle(x,y+1);
				
				Particle *ptemp5=getParticle(x-1,y);
				Particle *ptemp6=getParticle(x+1,y+2);
				
				
				cont=0;
				for(iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
					if(iterConstraint->isMember(ptemp1) && iterConstraint->isMember(ptemp2)){
						cont+=1;
						//			constraints.erase(iterConstraint);
						iterConstraint->desactivar();
					}		
					if(iterConstraint->isMember(ptemp3) && iterConstraint->isMember(ptemp4)){
						cont+=1;
						//			constraints.erase(iterConstraint);
						iterConstraint->desactivar();
					}	
					if(iterConstraint->isMember(ptemp5) && iterConstraint->isMember(ptemp6)){
						cont+=1;
						//			constraints.erase(iterConstraint);
						iterConstraint->desactivar();
					}						
				}
				if(cl_info)std::cout<<"contador no afecta p diagonal especial "<<cont<<"\n";
				
			}catch(...){
				if(cl_info)std::cout<<"Error en seleccion de algun tipo\n";
			}
			
		}
		
		
		if(x==xant-1 && y==yant+1){
			if(cl_info)std::cout<<"baje diagonal hacia izquierda\n";
			try{
			Particle *ptemp1=getParticle(x,y-1);
			Particle *ptemp2=getParticle(x+1,y);
			
			Particle *ptemp3=getParticle(x-1,y-2);
			Particle *ptemp4=getParticle(x+1,y);
			
			Particle *ptemp5=getParticle(x,y-1);
			Particle *ptemp6=getParticle(x+2,y+1);
			
			
			cont=0;
			for(iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
				if(iterConstraint->isMember(ptemp1) && iterConstraint->isMember(ptemp2)){
					cont+=1;
					//			constraints.erase(iterConstraint);
					iterConstraint->desactivar();
				}		
				if(iterConstraint->isMember(ptemp3) && iterConstraint->isMember(ptemp4)){
					cont+=1;
					//			constraints.erase(iterConstraint);
					iterConstraint->desactivar();
				}	
				if(iterConstraint->isMember(ptemp5) && iterConstraint->isMember(ptemp6)){
					cont+=1;
					//			constraints.erase(iterConstraint);
					iterConstraint->desactivar();
				}	
			}
			if(cl_info)std::cout<<"contador no afecta p diagonal especial "<<cont<<"\n";
			
			}catch(...){
				if(cl_info)std::cout<<"Error en seleccion de algun tipo\n";
			}
		}
		
	}else{//condiciones de borde
		
		if(x==0 and (y!=0 and y!=num_particles_height-1)){
			if(cl_info)std::cout<<"condicion de borde izquierdo sin extremos\n";				
			Particle *ptemp1 =getParticle(x,y+1);
			Particle *ptemp2 =getParticle(x,y-1);
			Particle *ptemp3,*ptemp4;
			
			try{
				cont=0;
				for(iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
					if(iterConstraint->isMember(ptemp1) && iterConstraint->isMember(ptemp2)){
						cont+=1;					
						iterConstraint->desactivar();
					}					
					if(x==xant-1 and y==yant+1){
						ptemp3=getParticle(x+1,y);
						ptemp4=getParticle(x+2,y+1);
						if(iterConstraint->isMember(ptemp3) && iterConstraint->isMember(ptemp2)){
							cont+=1;					
							iterConstraint->desactivar();
						}			
						if(iterConstraint->isMember(ptemp4) && iterConstraint->isMember(ptemp2)){
							cont+=1;					
							iterConstraint->desactivar();
						}
					}			
				}
				if(cl_info)std::cout<<"contador borde "<<cont<<"\n";
			}catch(...){
				if(cl_info)std::cout<<"Error de algun tipo\n";
			}			
		}
		
		
		if(x==num_particles_width-1 and (y!=0 and y!=num_particles_height-1)){
			if(cl_info)std::cout<<"condicion de borde derecho sin extremos\n";	
			Particle *ptemp1 =getParticle(x,y+1);
			Particle *ptemp2 =getParticle(x,y-1);
			Particle *ptemp3,*ptemp4;			
			
			
			try{
				cont=0;
				for(iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
					if(iterConstraint->isMember(ptemp1) && iterConstraint->isMember(ptemp2)){
						cont+=1;						
						iterConstraint->desactivar();
					}				
					if(x==xant+1 and y==yant-1){
						ptemp3=getParticle(x-1,y);
						ptemp4=getParticle(x-2,y-1);			
						if(iterConstraint->isMember(ptemp3) && iterConstraint->isMember(ptemp2)){
							cont+=1;						
							iterConstraint->desactivar();
						}			
						if(iterConstraint->isMember(ptemp4) && iterConstraint->isMember(ptemp2)){
							cont+=1;						
							iterConstraint->desactivar();
						}			
					}
				}
				if(cl_info)std::cout<<"contador borde "<<cont<<"\n";
			}catch(...){
				if(cl_info)std::cout<<"Error de algun tipo\n";
			}					
		}
		
		
		if(y==0 and (x!=0 and x!=num_particles_width-1) ){
			if(cl_info)std::cout<<"condicion de borde superior sin extremos\n";	
			Particle *ptemp1 =getParticle(x+1,y);
			Particle *ptemp2 =getParticle(x-1,y);
			Particle *ptemp3,*ptemp4;
			

			try{
				cont=0;
				for(iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
					if(iterConstraint->isMember(ptemp1) && iterConstraint->isMember(ptemp2)){
						cont+=1;
						//			constraints.erase(iterConstraint);
						iterConstraint->desactivar();
					}		
					//subir
					if(x==xant+1 and y==yant-1){
						ptemp3=getParticle(x,y+1);
						ptemp4=getParticle(x+1,y+2);
						if(iterConstraint->isMember(ptemp2) && iterConstraint->isMember(ptemp3)){
							cont+=1;					
							iterConstraint->desactivar();
						}	
						if(iterConstraint->isMember(ptemp4) && iterConstraint->isMember(ptemp2)){
							cont+=1;					
							iterConstraint->desactivar();
						}	
					}
				}
				if(cl_info)std::cout<<"contador borde "<<cont<<"\n";
			}catch(...){
				if(cl_info)std::cout<<"Error de algun tipo\n";
			}			
		}
		
		
		if(y==num_particles_height-1 and (x!=0 and x!=num_particles_width-1) ){
			if(cl_info)std::cout<<"condicion de borde inferior sin extremos\n";	
			Particle *ptemp1 =getParticle(x+1,y);
			Particle *ptemp2 =getParticle(x-1,y);
			Particle *ptemp3,*ptemp4;
			
			try{
				cont=0;
				for(iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
					if(iterConstraint->isMember(ptemp1) && iterConstraint->isMember(ptemp2)){
						cont+=1;					
						iterConstraint->desactivar();
					}	
					//bajar
					if(x==xant-1 and y==yant+1){
						ptemp3=getParticle(x,y-1);
						ptemp4=getParticle(x-1,y-2);
						if(iterConstraint->isMember(ptemp1) && iterConstraint->isMember(ptemp3)){
							cont+=1;					
							iterConstraint->desactivar();
						}	
						if(iterConstraint->isMember(ptemp4) && iterConstraint->isMember(ptemp1)){
							cont+=1;					
							iterConstraint->desactivar();
						}	
					}
				}
				if(cl_info)std::cout<<"contador borde "<<cont<<"\n";
			}catch(...){
				if(cl_info)std::cout<<"Error de algun tipo\n";
			}			
		}		
	}		
	//actualizo clic anterior
	xant=x;
	yant=y;
}


Particle* Cloth::BuscarParticula(Vec3 vparam,int &xp,int &yp){ //vparam clic en 3D, xp yp retornar posicion malla
	Particle* p = NULL;		
	float distanciaMenor = 999.9;	
	for(int x=0; x<num_particles_width; x++){		
		for(int y=0; y<num_particles_height; y++){
			Vec3 vp=getParticle(x, y)->getPos(); //vp posicion particula 3D
			vp[2]=0;vparam[2]=0;
			float temp=(vp-vparam).length();//distancia entre clic y particula actual de la malla
			if(/*0.5f > temp &&*/ temp <= distanciaMenor){
				distanciaMenor = temp;
				p=getParticle(x,y);
				if(p->isDrawlable()){
					xp=x;yp=y;
				}
			}
		}
	}		
	return p;
}

std::vector<Constraint>::iterator Cloth::getConstraint(Particle *p1, Particle *p2){
	std::vector<Constraint>::iterator iterConstraint;
	
	for (iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
		if( (iterConstraint->isMember(p1)) && (iterConstraint->isMember(p2))){
			return iterConstraint;
		}
	}
	return iterConstraint;
}


std::vector<Constraint>::iterator Cloth::getConstraint(Particle *p1){
	std::vector<Constraint>::iterator iterConstraint;
	
	for (iterConstraint = constraints.begin(); iterConstraint != constraints.end(); iterConstraint++){
		if(iterConstraint->isMember(p1)){
			return iterConstraint;
		}
	}
	return iterConstraint;
}

