#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

#define MAX_PARTICULAS  5000
#define floatrand() ((float)rand()/RAND_MAX)

// variables varias
int w = 800, h= 600,	//tama�os ventanas h x w
	metodo = 0,			//metodo seleccionado
	cant_metodos = 3;	//cantidad de metodos

//estructura de 1 particula 
typedef struct {
	float	life;		// vida
	float	r, g, b;    // color
	GLfloat x, y, z;    // posicion
	GLfloat vx, vy, vz; // velocidad 
    GLfloat ax, ay, az; // aceleracion
  public:
	void reset(){
		//posicion inicial de cada particula
		//el 0 0 0 esta en el centro de la ventana por como hice set de vista de la camara
		x = 0.0f;
		y = 0.0f;
		z = 0.0f;	
		//velocidad 
		/************************************************************************
		WEP WEP WEP WEP LEER ACA PARA MODIFICAR, TENGO SUE�O Y ME VOY A DORMIR
		HAGAS 1 O 1000 PARTICULAS TODAS INICIAN EN EL MISMO PUNTO, CON MISMA VELOCIDA 
		Y MISMA ACELERACION, POR LO QUE SIGUEN TODAS LA MISMA TRAYECTORIA
		ENTONCES TE TOPAS CON EL PROBLEMA QUE SE VE COMO SI FUERA UNA SOLA particula
		
		HAY QUE RANDOMIZAR LAS VELOCIDADES Y QUIZAS LAS ACELERACIONES PARA QUE SE 
		DISPERCEN UNAS DE OTRAS, CON VARIAR LAS VELOCIDADES DEBERIA BASTAR
		
		SI SE RANDOMIZA LOS COLORES EVITANDO CAER EN NEGRO O SIMILARES CADA PARTICULA 
		TENDRA COLOR DIFERENTE
		************************************************************************/
		vx = ((float) rand() / RAND_MAX);
		vy = ((float) rand() / RAND_MAX);
		vz = ((float) rand() / RAND_MAX);
		//aceleracion
		int i = (int)floatrand()%2+1;
		ax = pow(-.1f,i)*((float) rand() / RAND_MAX);//0.01f;
		ay = pow(-.04f,i)*((float) rand() / RAND_MAX);//-0.04f;
		az = pow(-.01f,i)*((float) rand() / RAND_MAX);//((float) rand() / RAND_MAX);// 0.0f;
		//color de la particula 
		r = ((float) rand() / RAND_MAX);
		g = ((float) rand() / RAND_MAX);
		b = ((float) rand() / RAND_MAX);
		//vida de la particula (depues juega en alpha)
		life = 1.0f;
	}
} Particle;

//arreglo de particulas
Particle particula[MAX_PARTICULAS];

//inicializo particulas y hago su comportamiento 
//aca no muestro nada en pantalla, solo calculo sus movimientos
void iniParticulas(void)
{
	srand(time(NULL));
	cout<<"Iniciando Particulas (cantidad = "<<MAX_PARTICULAS<<" \n";
	for(int i=0; i<MAX_PARTICULAS; i++) //recorro todas las particulas	 
	{
		particula[i].reset();
	}
}

//Inicializacion de OPENGL
void Inicializacion(void)
{
	glClearColor(0.0f,0.0f,0.0f,0.0f);     //fondo negro
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER,.5f);
	glDisable(GL_DEPTH_TEST);       	    //saco el ztest no me sirve en 2d
	iniParticulas();						//con esto apenas abre arrancan las particulas
}


//callback display
void display(void){
	//buena costumbre al hacer display limpiar buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	//void gluLookAt(eyeX,eyeY,eyeZ,centerX,centerY,centerZ,upX,upY,upZ); 
	gluLookAt (0.0, 0.0, 300.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	for (int i=0; i<MAX_PARTICULAS; i++){
	//metodos!
	switch(metodo) {
		case 0:
			particula[i].vx += particula[i].ax;
			particula[i].vy += particula[i].ay;
			particula[i].vz = 0.0f;
			break;
		case 1:
			particula[i].vx += -particula[i].ax;
			particula[i].vy += particula[i].ay;
			particula[i].vz = 0.0f;
			break;
		case 2: //se queda por copado
			float ang = rand()%361;
			particula[i].vx += cos(ang);//floatrand();
			particula[i].vy += sin(ang);
			particula[i].vz = 0;
			break;
		}
	
	//actualizo posiciones y velocidades
	particula[i].x += particula[i].vx;
	particula[i].y += particula[i].vy;
	particula[i].z += particula[i].vz;
	particula[i].life -= 0.005f;
	if ((particula[i].x > w/2) or (particula[i].x < -w/2) or (particula[i].y > h/2) or (particula[i].y < -h/2))
		particula[i].reset();
 
	//tomo el color de la particula
	glColor4f(particula[i].r,particula[i].g,particula[i].b, particula[i].life);
	glBegin(GL_QUADS);	//hago un cuadrado para verla (explicacion abajo)
		//z siempre esta en 0 estoy en 2D
		glVertex3f(particula[i].x+1.0f,particula[i].y+1.0f,particula[i].z);
		glVertex3f(particula[i].x+1.0f,particula[i].y-1.0f,particula[i].z);
		glVertex3f(particula[i].x-1.0f,particula[i].y-1.0f,particula[i].z);
		glVertex3f(particula[i].x-1.0f,particula[i].y+1.0f,particula[i].z);
		/**************************************************************************
		COMO TOME LOS PUNTOS ANTERIORES EN EL CUADRO
		
		v1--v2        donde v es posicion en el struct 
		| V  |        sumo y resto 1 para tener un "area" asi dibujo, por eso
		v3--v4		  el cuadrado es de 2x2
		***************************************************************************/
	glEnd();
	}
	
	glutSwapBuffers();//dibujo en el buffer de atras, hago el swap para ver el cambio
}

//callback del idle
void idle(void)
{
	glutPostRedisplay();
}

//callback del reshape
void reshape(int w, int h)
{
	glViewport(0, 0,  (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,(GLfloat)w/(GLfloat)h,0.1f,400.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

//Callback de TECLADO
void keyboard(unsigned char key, int x, int y)
{
 switch (key)
   {
    case 'E':
	case 'e': // de empezar
		iniParticulas();
		cout<<"Presiono E - iniciando particulas"<<endl;
		break;
	case 'M':
	case 'm': // metodo para mostrar
	   ++metodo;
	   if (metodo == cant_metodos)
		   metodo = 0;
	   iniParticulas();
	   cout<<"Cambio de metodo - "<<metodo<<endl;
	   break;
	case 'q':
	case 'Q':
	case 27: exit(0);
		break;
   }
}

void help(){ // un lindo menu en consola :P
	cout<<"TRABAJO INTEGRADOR DE COMPUTACION GRAFICA"<<endl;
	cout<<"\n\tTeclas\n";
	cout<<"\t e   - inicializar\n";
	cout<<"\t Esc - Salir\n";
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	cout<<"Iniciando programa con argumentos: "<<&argc<<" - "<<argv<<"endl";
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB); 	//establezco modos de display
	glutInitWindowSize (w, h);						//tama�o ventana
	cout<<"Ventana de "<<w<<" - "<<h<<"endl";
	glutInitWindowPosition (10,10);				//posicion inicial ventana
	glutCreateWindow ("Sistema de Particulas");	//titulo ventana
	Inicializacion();								//inicializo parametros de OPENGL
	cout<<"arrancan las definiciones de callbacks"<<endl;
	glutDisplayFunc(display);						//asocio el display (visualizacion)
	glutReshapeFunc(reshape);						//asocio el reshape (remodelado)
	glutKeyboardFunc(keyboard);					//asocio el teclado
	glutIdleFunc(idle);							//asocio el modo IDLE (cdo esta desocupado, tipo standby)

	help();

	cout<<"Entrando en Loop de OPENGL"<<endl;
	glutMainLoop();								//arranco el loop de OPENGL
	return 0;
}
