#include "Box.h"

void Box::Draw(){
	
	if (this->activo) {
		glPushMatrix(); 
		
		glScaled(dimension[0],dimension[1],dimension[2]);
		glTranslatef(this->position[0],this->position[1],this->position[2]);		
		glColor4f(color[0],color[1],color[2],0.2f);	

		glutSolidCube(1.f);
		glPopMatrix();
	}
	
};


